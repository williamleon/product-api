package com.prototype.productapi.infrastructure.adapter.outbound.stream;

import com.prototype.productapi.domain.kernel.event.user.ProductEvent;
import com.prototype.productapi.domain.usecase.stream.ProductBinding;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class StreamProductBinding implements ProductBinding {

	private final OutputBinding outputBinding;

	@Override public void send(final ProductEvent productEvent) {
		log.info(String.format("Sending event for product %s", productEvent.getProduct().getId()));
		outputBinding.productChannel()
					 .send(MessageBuilder
								   .withPayload(productEvent)
								   .build());
	}
}
