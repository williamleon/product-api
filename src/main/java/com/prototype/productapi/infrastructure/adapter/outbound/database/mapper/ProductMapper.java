package com.prototype.productapi.infrastructure.adapter.outbound.database.mapper;

import java.util.Optional;

import com.prototype.productapi.domain.kernel.model.ImmutableProduct;
import com.prototype.productapi.domain.kernel.model.Product;
import com.prototype.productapi.infrastructure.adapter.outbound.database.model.DBProduct;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
public class ProductMapper {

	public static final Product fromDBProduct(DBProduct product) {

		return ImmutableProduct.builder()
							   .id(product.getId())
							   .reference(product.getReference())
							   .name(product.getName())
							   .price(product.getPrice())
							   .cantInStock(product.getCantInStock())
							   .cantInHold(product.getCantInHold())
							   .description(Optional.ofNullable(product.getDescription()))
							   .urlImage(Optional.ofNullable(product.getUrlImage()))
							   .category(Optional.ofNullable(product.getCategory()))
							   .build();
	}

	public static final DBProduct fromProduct(Product product) {

		return DBProduct.builder()
						.id(product.getId().orElse(null))
						.reference(product.getReference())
						.name(product.getName())
						.price(product.getPrice())
						.cantInStock(product.getCantInStock())
						.cantInHold(product.getCantInHold())
						.description(product.getDescription().orElse(null))
						.urlImage(product.getUrlImage().orElse(null))
						.category(product.getCategory().orElse(null))
						.build();
	}
}
