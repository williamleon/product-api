package com.prototype.productapi.infrastructure.adapter.inbound.stream;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
public interface InputBinding {

	String ORDER_MESSAGE = "purchase-order";

	@Input(ORDER_MESSAGE)
	SubscribableChannel registryChannel();
}
