package com.prototype.productapi.infrastructure.adapter.inbound.stream.listener;

import com.prototype.productapi.domain.kernel.command.CommandBus;
import com.prototype.productapi.domain.kernel.command.product.UpdateProductByOrderCommand;
import com.prototype.productapi.infrastructure.adapter.inbound.stream.InputBinding;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Component;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class OrderListener {

	public final CommandBus bus;

	@StreamListener(target = InputBinding.ORDER_MESSAGE)
	public void updateProductAvailability(UpdateProductByOrderCommand command) {

		log.info(String.format("Listen purchase-order event for %s", command.getOrder().getId()));
		bus.dispatch(command);
	}
}
