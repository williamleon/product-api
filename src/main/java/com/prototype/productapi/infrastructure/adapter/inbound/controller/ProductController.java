package com.prototype.productapi.infrastructure.adapter.inbound.controller;

import static com.prototype.productapi.infrastructure.adapter.inbound.controller.ResponseUtil.getMessage;

import java.util.List;

import com.prototype.productapi.domain.kernel.model.Product;
import com.prototype.productapi.domain.kernel.model.ProductResume;
import com.prototype.productapi.domain.kernel.query.QueryBus;
import com.prototype.productapi.domain.kernel.query.product.ImmutableProductDetailQuery;
import com.prototype.productapi.domain.kernel.query.product.ImmutableProductResumeQuery;
import io.vavr.control.Either;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@CrossOrigin(origins="*")
@RequestMapping("/product")
public class ProductController {

	private final QueryBus queryBus;

	@GetMapping
	public ResponseEntity<?> list() {

		Either<Throwable, List<ProductResume>> execution = queryBus.ask(ImmutableProductResumeQuery.builder().build());
		return execution.map(ResponseEntity::ok)
						.getOrElseGet(throwable -> new ResponseEntity(getMessage(throwable.getMessage()), HttpStatus.CONFLICT));

	}

	@GetMapping("/{id}")
	public ResponseEntity<?> getDetail(@PathVariable String id) {

		Either<Throwable, Product> execution = queryBus.ask(ImmutableProductDetailQuery.builder().id(id).build());
		return execution.map(ResponseEntity::ok)
						.getOrElseGet(throwable -> new ResponseEntity(getMessage(throwable.getMessage()), HttpStatus.UNAUTHORIZED));
	}

}
