package com.prototype.productapi.infrastructure.adapter.inbound.controller;

import static com.prototype.productapi.infrastructure.adapter.inbound.controller.ResponseUtil.getMessage;

import javax.validation.Valid;

import com.prototype.productapi.domain.kernel.command.CommandBus;
import com.prototype.productapi.domain.kernel.command.product.CreateProductCommand;
import com.prototype.productapi.domain.kernel.command.product.UpdateProductCommand;
import com.prototype.productapi.domain.kernel.model.Product;
import io.vavr.control.Either;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("admin/product")
public class ManageProductController {

	private final CommandBus commandBus;

	@PostMapping
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<?> create(@Valid @RequestBody CreateProductCommand command) {

		Either<Throwable, Product> execution = commandBus.dispatch(command);
		return execution.map(product -> ResponseEntity.noContent().build())
						.getOrElseGet(throwable -> new ResponseEntity<>(getMessage(throwable.getMessage()), HttpStatus.CONFLICT));
	}

	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PutMapping
	public ResponseEntity<?> update(@Valid @RequestBody UpdateProductCommand command) {

		Either<Throwable, Product> execution = commandBus.dispatch(command);
		return execution.map(product -> ResponseEntity.noContent().build())
						.getOrElseGet(throwable -> new ResponseEntity(getMessage(throwable.getMessage()), HttpStatus.BAD_REQUEST));
	}
}
