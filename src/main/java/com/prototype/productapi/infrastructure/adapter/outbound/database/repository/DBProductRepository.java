package com.prototype.productapi.infrastructure.adapter.outbound.database.repository;

import com.prototype.productapi.infrastructure.adapter.outbound.database.model.DBProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Repository
public interface DBProductRepository extends JpaRepository<DBProduct, String> {

}
