package com.prototype.productapi.infrastructure.adapter.outbound.database.repository;

import static com.prototype.productapi.infrastructure.adapter.outbound.database.mapper.ProductMapper.fromDBProduct;
import static com.prototype.productapi.infrastructure.adapter.outbound.database.mapper.ProductMapper.fromProduct;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.prototype.productapi.domain.kernel.model.Product;
import com.prototype.productapi.domain.usecase.repository.ProductRepository;
import com.prototype.productapi.infrastructure.adapter.outbound.database.mapper.ProductMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Component
@RequiredArgsConstructor
public class MapperProductRepository implements ProductRepository {

	private final DBProductRepository dbProductRepository;

	@Override public Optional<Product> findById(final String id) {

		return dbProductRepository.findById(id).map(ProductMapper::fromDBProduct);
	}

	@Override public List<Product> findAll() {

		return dbProductRepository.findAll().stream()
								  .map(ProductMapper::fromDBProduct)
								  .collect(Collectors.toList());
	}

	@Override public List<Product> findAll(List<String> ids) {

		return dbProductRepository.findAllById(ids).stream()
								  .map(ProductMapper::fromDBProduct)
								  .collect(Collectors.toList());
	}

	@Override public Product save(final Product product) {

		return fromDBProduct(dbProductRepository.save(fromProduct(product)));
	}

	@Override public List<Product> saveAll(final List<Product> products) {

		return dbProductRepository
				.saveAll(products.stream()
								 .map(ProductMapper::fromProduct)
								 .collect(Collectors.toList()))
				.stream().map(ProductMapper::fromDBProduct)
				.collect(Collectors.toList());
	}
}
