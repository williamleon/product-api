package com.prototype.productapi.infrastructure.assembler;

import com.prototype.productapi.domain.kernel.command.CommandExecute;
import com.prototype.productapi.domain.kernel.command.product.ImmutableCreateProductCommand;
import com.prototype.productapi.domain.kernel.command.product.ImmutableUpdateProductByOrderCommand;
import com.prototype.productapi.domain.kernel.command.product.ImmutableUpdateProductCommand;
import com.prototype.productapi.domain.kernel.event.EventHandler;
import com.prototype.productapi.domain.kernel.event.user.ImmutableProductEvent;
import com.prototype.productapi.domain.kernel.query.QueryHandler;
import com.prototype.productapi.domain.kernel.query.product.ImmutableProductDetailQuery;
import com.prototype.productapi.domain.kernel.query.product.ImmutableProductResumeQuery;
import com.prototype.productapi.domain.usecase.LocalCommandBus;
import com.prototype.productapi.domain.usecase.LocalEventBus;
import com.prototype.productapi.domain.usecase.LocalQueryBus;
import io.vavr.collection.HashMap;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure Query/Command bus
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Configuration
public class BusConfig {

	@Bean
	public LocalCommandBus commandBus(CommandExecute updateProductCommandExecute,
									  CommandExecute createProductCommandExecute,
									  CommandExecute updateProductByOrderCommandExecute) {

		return new LocalCommandBus(HashMap.of(ImmutableUpdateProductCommand.class.getName(), updateProductCommandExecute,
											  ImmutableCreateProductCommand.class.getName(), createProductCommandExecute,
											  ImmutableUpdateProductByOrderCommand.class.getName(), updateProductByOrderCommandExecute));
	}

	@Bean
	public LocalQueryBus queryBus(QueryHandler productDetailQueryHandler,
								  QueryHandler productResumeQueryHandler) {

		return new LocalQueryBus(HashMap.of(ImmutableProductDetailQuery.class.getName(), productDetailQueryHandler,
											ImmutableProductResumeQuery.class.getName(), productResumeQueryHandler));
	}

	@Bean
	public LocalEventBus eventBus(EventHandler productEventHandler) {

		return new LocalEventBus(HashMap.of(ImmutableProductEvent.class.getName(), productEventHandler));
	}
}
