package com.prototype.productapi.domain.usecase.service;

import com.prototype.productapi.domain.kernel.event.EventHandler;
import com.prototype.productapi.domain.kernel.event.user.ProductEvent;
import com.prototype.productapi.domain.usecase.stream.ProductBinding;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ProductEventHandler implements EventHandler<ProductEvent> {

	private final ProductBinding productBinding;

	@Override public void handle(final ProductEvent event) {

		log.info(String.format("handling ProductEvent for %s", event.getProduct().getId()));
		productBinding.send(event);
	}
}
