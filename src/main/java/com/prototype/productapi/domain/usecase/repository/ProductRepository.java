package com.prototype.productapi.domain.usecase.repository;

import java.util.List;
import java.util.Optional;

import com.prototype.productapi.domain.kernel.model.Product;

/**
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
public interface ProductRepository {

	Optional<Product> findById(String id);

	List<Product> findAll();

	Product save(Product product);

	List<Product> findAll(List<String> ids);

	List<Product> saveAll(final List<Product> products);
}
