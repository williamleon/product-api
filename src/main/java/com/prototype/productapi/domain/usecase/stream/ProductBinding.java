package com.prototype.productapi.domain.usecase.stream;

import com.prototype.productapi.domain.kernel.event.user.ProductEvent;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
public interface ProductBinding {

	void send(ProductEvent productEvent);
}
