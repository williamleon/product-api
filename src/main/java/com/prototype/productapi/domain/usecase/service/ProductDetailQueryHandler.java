package com.prototype.productapi.domain.usecase.service;

import com.prototype.productapi.domain.kernel.exception.ProductException;
import com.prototype.productapi.domain.kernel.model.Product;
import com.prototype.productapi.domain.kernel.query.QueryHandler;
import com.prototype.productapi.domain.kernel.query.product.ProductDetailQuery;
import com.prototype.productapi.domain.usecase.repository.ProductRepository;
import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Query handler implementation to load product detail information
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ProductDetailQueryHandler implements QueryHandler<ProductDetailQuery, Either<Throwable, Product>> {

	private static final String MESSAGE = "Getting error loading product information";
	private final ProductRepository productRepository;

	/**
	 * Load product information base on id contained inside query To control the possible side effects is used {@link Either} structure, in
	 * case of a success process the product loaded is returned in the right side of {@link Either}
	 *
	 * @param query
	 * @return
	 */
	@Override public Either<Throwable, Product> handle(final ProductDetailQuery query) {

		log.info("Starting product detail query...");
		return Try.of(() -> productRepository.findById(query.getId()).get())
				  .peek(product -> log.info(String.format("Product %s found in system", product.getId())))
				  .onFailure(throwable -> log.error(MESSAGE, throwable))
				  .toEither(() -> new ProductException(MESSAGE));
	}
}
