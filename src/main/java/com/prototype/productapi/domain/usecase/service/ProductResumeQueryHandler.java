package com.prototype.productapi.domain.usecase.service;

import java.util.List;
import java.util.stream.Collectors;

import com.prototype.productapi.domain.kernel.exception.ProductException;
import com.prototype.productapi.domain.kernel.model.ProductResume;
import com.prototype.productapi.domain.kernel.model.mapper.Mapper;
import com.prototype.productapi.domain.kernel.query.QueryHandler;
import com.prototype.productapi.domain.kernel.query.product.ProductResumeQuery;
import com.prototype.productapi.domain.usecase.repository.ProductRepository;
import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ProductResumeQueryHandler implements QueryHandler<ProductResumeQuery, Either<Throwable, List<ProductResume>>> {

	private static final String MESSAGE = "Getting error loading product information";
	private final ProductRepository productRepository;

	/**
	 * Load all product information and map all record to {@link ProductResume}. To control the possible side effects is used {@link Either}
	 * structure, in case of a success process the products loaded are returned in the right side of {@link Either}
	 *
	 * @param query
	 * @return
	 */
	@Override public Either<Throwable, List<ProductResume>> handle(final ProductResumeQuery query) {

		log.info("Starting product resume query...");
		return Try.of(productRepository::findAll)
				  .peek(products -> log.info(String.format("Load %s products from system", products.size())))
				  .map(products -> products.stream().map(Mapper::fromProduct).collect(Collectors.toList()))
				  .onFailure(throwable -> log.error(MESSAGE, throwable))
				  .toEither(() -> new ProductException(MESSAGE));
	}
}
