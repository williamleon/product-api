package com.prototype.productapi.domain.usecase.service;

import com.prototype.productapi.domain.kernel.command.CommandExecute;
import com.prototype.productapi.domain.kernel.command.product.UpdateProductCommand;
import com.prototype.productapi.domain.kernel.event.EventBus;
import com.prototype.productapi.domain.kernel.event.user.ImmutableProductEvent;
import com.prototype.productapi.domain.kernel.exception.ProductException;
import com.prototype.productapi.domain.kernel.model.ImmutableProduct;
import com.prototype.productapi.domain.kernel.model.Product;
import com.prototype.productapi.domain.usecase.repository.ProductRepository;
import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Command execute implementation to update product
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class UpdateProductCommandExecute implements CommandExecute<UpdateProductCommand, Either<Throwable, Product>> {

	private static final String MESSAGE = "Getting error updating product";
	private final ProductRepository productRepository;
	private final EventBus eventBus;

	/**
	 * Before to update product information, this execution validate if given id product exist in system. To control the possible side
	 * effects is used {@link Either} structure, in case of a success process the product created is returned in the right side of {@link
	 * Either}
	 *
	 * @param command
	 * @return
	 */
	@Override public Either<Throwable, Product> execute(final UpdateProductCommand command) {

		log.info("Starting update product command...");
		return Try.of(() -> productRepository.findById(command.getId()).get())
				  .peek(product -> log.info(String.format("Product %s found in system", product.getId())))
				  .map(product -> (Product) ImmutableProduct.copyOf(product)
															.withReference(command.getReference())
															.withName(command.getName())
															.withPrice(command.getPrice())
															.withCantInStock(command.getCantInStock())
															.withDescription(command.getDescription())
															.withUrlImage(command.getUrlImage())
															.withCategory(command.getCategory()))
				  .transform(products -> Try.of(() -> productRepository.save(products.get())))
				  .peek(product -> log.info(String.format("Product %s updated successfully", product.getId())))
				  .peek(product -> eventBus.emit(ImmutableProductEvent.builder().product(product).build()))
				  .onFailure(throwable -> log.error(MESSAGE, throwable))
				  .toEither(() -> new ProductException(MESSAGE));
	}
}
