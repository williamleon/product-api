package com.prototype.productapi.domain.usecase.service;

import static io.vavr.API.$;
import static io.vavr.API.Case;
import static io.vavr.API.Match;

import java.util.List;
import java.util.stream.Collectors;

import com.prototype.productapi.domain.kernel.command.CommandExecute;
import com.prototype.productapi.domain.kernel.command.product.UpdateProductByOrderCommand;
import com.prototype.productapi.domain.kernel.command.product.model.Order;
import com.prototype.productapi.domain.kernel.command.product.model.OrderProduct;
import com.prototype.productapi.domain.kernel.event.EventBus;
import com.prototype.productapi.domain.kernel.event.user.ImmutableProductEvent;
import com.prototype.productapi.domain.kernel.exception.ProductException;
import com.prototype.productapi.domain.kernel.model.ImmutableProduct;
import com.prototype.productapi.domain.kernel.model.Product;
import com.prototype.productapi.domain.usecase.repository.ProductRepository;
import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class UpdateProductByOrderCommandExecute implements CommandExecute<UpdateProductByOrderCommand, Either<Throwable, List<Product>>> {

	public static final String CREATED = "CREATED";
	public static final String REVERSED = "REVERSED";
	public static final String COMPLETED = "COMPLETED";
	public static final String CANCELED = "CANCELED";
	private static final String MESSAGE = "Getting error updating product";

	private final ProductRepository productRepository;
	private final EventBus eventBus;

	/**
	 * Retrieve all products from system related to the purchase order {@link Order} and depending on order status update the stock. To
	 * control the possible side effects is used {@link Either} structure, in case of a success process the products updated are returned in
	 * the right side of {@link Either}
	 *
	 * @param command {@link UpdateProductByOrderCommand} command with the purchase order {@link Order} information
	 * @return {@link Either}
	 */
	@Override public Either<Throwable, List<Product>> execute(final UpdateProductByOrderCommand command) {

		log.info(String.format("Starting update product command  by order [%s][%s]...", command.getOrder().getId(),
							   command.getOrder().getStatus()));

		return Try.of(command::getOrder)
				  .transform(orders -> Try.of(() -> productRepository.findAll(getOrderProductsId(orders.get())))
										  .map(products -> transformBaseOnOrderStatus(products, orders.get())))
				  .transform(lists -> Try.of(() -> productRepository.saveAll(lists.get())))
				  .peek(products -> products.forEach(p -> {
					  log.info(String.format("Product %s updated successfully", p.getId()));
					  eventBus.emit(ImmutableProductEvent.builder().product(p).build());
				  }))
				  .onFailure(throwable -> log.error(MESSAGE, throwable))
				  .toEither(() -> new ProductException(MESSAGE));
	}

	/**
	 * Update stock strategy base on {@link Order} status
	 *
	 * @param products list of products retrieve from system
	 * @param order    purchase information
	 * @return products with the stock updated
	 */
	private static List<Product> transformBaseOnOrderStatus(List<Product> products, Order order) {

		return Match(order.getStatus()).of(
				Case($(CREATED), holdProducts(products, order.getProducts())),
				Case($(REVERSED), cancelDeliverProducts(products, order.getProducts())),
				Case($(COMPLETED), deliverProducts(products, order.getProducts())),
				Case($(CANCELED), reverseHoldProducts(products, order.getProducts())),
				Case($(), products));
	}

	/**
	 * Update the product availability adding of hold the cant in the {@link OrderProduct}
	 *
	 * @param products      list of products retrieve from system
	 * @param orderProducts products in purchase
	 * @return products with the stock updated
	 */
	private static List<Product> holdProducts(List<Product> products, List<OrderProduct> orderProducts) {

		return products.stream()
					   .map(product -> ImmutableProduct
							   .copyOf(product)
							   .withCantInHold(product.getCantInHold()
													   + findOrderProductCantById(orderProducts, product.getId().orElse(null)))
						   ).collect(Collectors.toList());
	}

	/**
	 * Update the product availability removing of hold the cant in the {@link OrderProduct}
	 *
	 * @param products      list of products retrieve from system
	 * @param orderProducts products in purchase
	 * @return products with the stock updated
	 */
	private static List<Product> reverseHoldProducts(List<Product> products, List<OrderProduct> orderProducts) {

		return products.stream()
					   .map(product -> ImmutableProduct
							   .copyOf(product)
							   .withCantInHold(product.getCantInHold()
													   - findOrderProductCantById(orderProducts, product.getId().orElse(null)))
						   ).collect(Collectors.toList());
	}

	/**
	 * Update the product availability adding of stock the cant in the {@link OrderProduct}
	 *
	 * @param products      list of products retrieve from system
	 * @param orderProducts products in purchase
	 * @return products with the stock updated
	 */
	private static List<Product> cancelDeliverProducts(List<Product> products, List<OrderProduct> orderProducts) {

		return products.stream()
					   .map(product -> ImmutableProduct
							   .copyOf(product)
							   .withCantInStock(product.getCantInStock()
														+ findOrderProductCantById(orderProducts, product.getId().orElse(null)))
						   ).collect(Collectors.toList());
	}

	/**
	 * Update the product availability removing of stock and hold the cant in the {@link OrderProduct}
	 *
	 * @param products      list of products retrieve from system
	 * @param orderProducts products in purchase
	 * @return products with the stock updated
	 */
	private static List<Product> deliverProducts(List<Product> products, List<OrderProduct> orderProducts) {

		return products.stream()
					   .map(product -> {
								Integer cant = findOrderProductCantById(orderProducts, product.getId().orElse(null));
								return ImmutableProduct
										.copyOf(product)
										.withCantInStock(product.getCantInStock() - cant)
										.withCantInHold(product.getCantInHold() - cant);
							}
						   ).collect(Collectors.toList());
	}

	/**
	 * Retrieve the product cant in purchase order given a product id
	 *
	 * @param orderProducts products in purchase
	 * @param id            product id
	 * @return cant of purchase order product
	 */
	private static Integer findOrderProductCantById(List<OrderProduct> orderProducts, String id) {

		return orderProducts.stream()
							.filter(op -> id.equals(op.getId()))
							.map(OrderProduct::getCant).findFirst().orElse(null);
	}

	/**
	 * Retrieve all products id in given {@link Order}
	 *
	 * @param order purchase order information
	 * @return list products id
	 */
	private static List<String> getOrderProductsId(Order order) {

		return order.getProducts().stream().map(OrderProduct::getId).collect(Collectors.toList());
	}

}
