package com.prototype.productapi.domain.usecase.service;

import static com.prototype.productapi.domain.kernel.model.mapper.Mapper.fromCreateProductCommand;

import com.prototype.productapi.domain.kernel.command.CommandExecute;
import com.prototype.productapi.domain.kernel.command.product.CreateProductCommand;
import com.prototype.productapi.domain.kernel.event.EventBus;
import com.prototype.productapi.domain.kernel.event.user.ImmutableProductEvent;
import com.prototype.productapi.domain.kernel.exception.ProductException;
import com.prototype.productapi.domain.kernel.model.Product;
import com.prototype.productapi.domain.usecase.repository.ProductRepository;
import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Command execute implementation to create product
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class CreateProductCommandExecute implements CommandExecute<CreateProductCommand, Either<Throwable, Product>> {

	private static final String MESSAGE = "Getting error saving product";
	private final ProductRepository productRepository;
	private final EventBus eventBus;

	/**
	 * Perform save operation using {@link ProductRepository}. previously to perform this operation the command y mapped to product business
	 * object To control the possible side effects is used {@link Either} structure, in case of a success process the product created is
	 * returned in the right side of {@link Either}
	 *
	 * @param command Create product command
	 * @return Either
	 */
	@Override public Either<Throwable, Product> execute(final CreateProductCommand command) {

		log.info("Starting create product command...");
		return Try.of(() -> productRepository.save(fromCreateProductCommand(command)))
				  .peek(product -> log.info(String.format("Product created successfully ref: %s", product.getReference())))
				  .peek(product -> eventBus.emit(ImmutableProductEvent.builder().product(product).build()))
				  .onFailure(throwable -> log.error(MESSAGE, throwable))
				  .toEither(() -> new ProductException(MESSAGE));
	}
}
