package com.prototype.productapi.domain.kernel.query.product;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.prototype.productapi.domain.kernel.query.Query;
import org.immutables.value.Value;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Value.Immutable
@JsonDeserialize(as = ImmutableProductDetailQuery.class)
@JsonSerialize(as = ImmutableProductDetailQuery.class)
public interface ProductDetailQuery extends Query {

	String getId();
}
