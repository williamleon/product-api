package com.prototype.productapi.domain.kernel.command.product;

import java.util.Optional;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.prototype.productapi.domain.kernel.command.Command;
import org.immutables.value.Value;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Value.Style(get = {"is*", "get*"})
@JsonDeserialize(as = ImmutableUpdateProductCommand.class)
@JsonSerialize(as = ImmutableUpdateProductCommand.class)
@Value.Immutable
public interface UpdateProductCommand extends Command {

	String getId();

	String getReference();

	String getName();

	Double getPrice();

	Integer getCantInStock();

	Optional<String> getDescription();

	Optional<String> getUrlImage();

	Optional<String> getCategory();

}
