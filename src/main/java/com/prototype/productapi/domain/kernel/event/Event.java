package com.prototype.productapi.domain.kernel.event;

/**
 * Interface to identify events
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
public interface Event {

}
