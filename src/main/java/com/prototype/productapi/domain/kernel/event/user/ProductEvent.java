package com.prototype.productapi.domain.kernel.event.user;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.prototype.productapi.domain.kernel.event.Event;
import com.prototype.productapi.domain.kernel.model.Product;
import org.immutables.value.Value;

/**
 * Interface to identify product events
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Value.Immutable
@JsonDeserialize(as = ImmutableProductEvent.class)
@JsonSerialize(as = ImmutableProductEvent.class)
public interface ProductEvent extends Event {

	Product getProduct();
}
