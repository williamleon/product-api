package com.prototype.productapi.domain.kernel.command.product.model;

import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Value.Style(get = {"is*", "get*"})
@JsonDeserialize(as = ImmutableOrder.class, builder = ImmutableOrder.Builder.class)
@JsonSerialize(as = ImmutableOrder.class)
@Value.Immutable
public interface Order {

	String getId();

	String getStatus();

	List<OrderProduct> getProducts();
}
