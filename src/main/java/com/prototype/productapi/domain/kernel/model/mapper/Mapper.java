package com.prototype.productapi.domain.kernel.model.mapper;

import com.prototype.productapi.domain.kernel.command.product.CreateProductCommand;
import com.prototype.productapi.domain.kernel.command.product.UpdateProductCommand;
import com.prototype.productapi.domain.kernel.model.ImmutableProduct;
import com.prototype.productapi.domain.kernel.model.ImmutableProductResume;
import com.prototype.productapi.domain.kernel.model.Product;
import com.prototype.productapi.domain.kernel.model.ProductResume;
import lombok.NoArgsConstructor;

/**
 * Mapper util for business objects on business layer
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@NoArgsConstructor
public class Mapper {

	public static final ProductResume fromProduct (Product product){
		return ImmutableProductResume.builder()
									 .id(product.getId())
									 .reference(product.getReference())
									 .availability(product.getAvailability())
									 .name(product.getName())
									 .price(product.getPrice())
									 .urlImage(product.getUrlImage())
									 .build();
	}

	public static final Product fromCreateProductCommand (CreateProductCommand command) {
		return ImmutableProduct.builder()
							   .reference(command.getReference())
							   .name(command.getName())
							   .price(command.getPrice())
							   .cantInStock(command.getCantInStock())
							   .description(command.getDescription())
							   .urlImage(command.getUrlImage())
							   .category(command.getCategory())
							   .build();
	}

	public static final Product fromUpdateProductCommand(UpdateProductCommand command) {
		return ImmutableProduct.builder()
							   .id(command.getId())
							   .reference(command.getReference())
							   .name(command.getName())
							   .price(command.getPrice())
							   .cantInStock(command.getCantInStock())
							   .description(command.getDescription())
							   .urlImage(command.getUrlImage())
							   .category(command.getCategory())
							   .build();
	}
}
