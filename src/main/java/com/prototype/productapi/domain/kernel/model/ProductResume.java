package com.prototype.productapi.domain.kernel.model;

import java.util.Optional;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

/**
 * Business object to handle product resume
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Value.Immutable
@JsonDeserialize(as = ImmutableProductResume.class)
@JsonSerialize(as = ImmutableProductResume.class)
public interface ProductResume {

	Optional<String> getId();

	String getReference();

	String getName();

	Double getPrice();

	Integer getAvailability();

	Optional<String> getUrlImage();

}
