package com.prototype.productapi.domain.kernel.model;

import java.util.Optional;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

/**
 * Business object to handle product
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Value.Immutable
@JsonDeserialize(as = ImmutableProduct.class)
@JsonSerialize(as = ImmutableProduct.class)
public interface Product {

	Optional<String> getId();

	String getReference();

	String getName();

	Double getPrice();

	Integer getCantInStock();

	@Value.Default
	default Integer getCantInHold() {

		return 0;
	}

	@Value.Derived
	default Integer getAvailability() {

		return getCantInStock() - getCantInHold();
	}

	Optional<String> getDescription();

	Optional<String> getUrlImage();

	Optional<String> getCategory();
}
